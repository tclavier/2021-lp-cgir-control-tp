package fr.univlille.iut.cgir.api;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class HealthTest {
    @Test
    void should_show_redis_not_present() {
        String body = new Health().health();
        String redisLine = "";
        for (String line : body.split("\n")) {
            if (line.toLowerCase(Locale.ROOT).contains("redis")) redisLine = line;
        }
        assertEquals("* Redis : Not present",redisLine);
    }

    @Test
    void should_show_redis_bad_configuration() {
        Configuration.getInstance().setRedisHost("badhost");
        String body = new Health().health();
        String redisLine = "";
        for (String line : body.split("\n")) {
            if (line.toLowerCase(Locale.ROOT).contains("redis")) redisLine = line;
        }
        assertEquals("* Redis : Bad configuration",redisLine);
    }

}