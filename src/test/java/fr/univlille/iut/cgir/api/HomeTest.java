package fr.univlille.iut.cgir.api;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.ws.rs.core.Application;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HomeTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new Api();
    }

    @BeforeAll
    public void before() throws Exception {
        super.setUp();
    }

    @AfterAll
    public void after() throws Exception {
        super.tearDown();
    }

    @Test
    public void should_call_home_url() {
        int body = target("/").request().get().getStatus();
        assertEquals(200, body);
    }
}
