package fr.univlille.iut.cgir.domain;

import org.junit.jupiter.api.Test;

import java.security.InvalidParameterException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IntervalDomainTest {
    @Test
    void should_compute_interval_between_2_dates() {
        LocalDate startDate = LocalDate.of(2015, 3, 2);
        LocalDate endDate = LocalDate.of(2015, 3, 3);
        Long days = IntervalDomain.between(startDate, endDate);
        assertEquals(1, days);
    }

    @Test
    void should_compute_interval_for_more_than_one_month() {
        LocalDate startDate = LocalDate.of(2015, 3, 2);
        LocalDate endDate = LocalDate.of(2015, 4, 3);
        Long days = IntervalDomain.between(startDate, endDate);
        assertEquals(32, days);
    }

    @Test
    void should_compute_interval_for_more_than_one_year() {
        LocalDate startDate = LocalDate.of(2015, 3, 2);
        LocalDate endDate = LocalDate.of(2020, 4, 3);
        Long days = IntervalDomain.between(startDate, endDate);
        assertEquals(1859, days);
    }

    @Test
    void should_raise_an_error_when_endDate_is_before_startDate() {
        assertThrows(InvalidParameterException.class, () -> {
            LocalDate startDate = LocalDate.of(2025, 3, 2);
            LocalDate endDate = LocalDate.of(2020, 4, 3);
            IntervalDomain.between(startDate, endDate);
        });
    }

}