package fr.univlille.iut.cgir.api;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;

@XmlRootElement
public class DateXml {
    private String day;
    private String month;
    private String year;

    public static DateXml from(String day, String month, String year) {
        DateXml dateXml = new DateXml();
        dateXml.day = day;
        dateXml.month = month;
        dateXml.year = year;
        return dateXml;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public LocalDate toLocalDate() {
        return LocalDate.of(
                Integer.parseInt(year),
                Integer.parseInt(month),
                Integer.parseInt(day));
    }
}
