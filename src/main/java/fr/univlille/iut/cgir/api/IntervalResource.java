package fr.univlille.iut.cgir.api;

import fr.univlille.iut.cgir.domain.IntervalDomain;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.security.InvalidParameterException;
import java.time.DateTimeException;
import java.time.LocalDate;

@Path("/interval/")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class IntervalResource {

    @POST
    public IntervalXml computeInterval(BetweenXml betweenXml) {
        LocalDate startDate;
        try {
            startDate = betweenXml.getStart().toLocalDate();
        } catch (DateTimeException e) {
            return IntervalXml.from("Start date must be valid");
        }

        LocalDate endDate;
        try {
            endDate = betweenXml.getEnd().toLocalDate();
        } catch (DateTimeException e) {
            return IntervalXml.from("End date must be valid");
        }

        try {
            return IntervalXml.from(IntervalDomain.between(startDate, endDate));
        } catch (InvalidParameterException e) {
            return IntervalXml.from("End date must be after start date");
        }
    }

    @GET
    public IntervalXml get() {
        return IntervalXml.from("Message", 1);
    }
}
