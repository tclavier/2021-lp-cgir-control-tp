package fr.univlille.iut.cgir.api;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BetweenXml {
    private DateXml end;
    private DateXml start;

    public static BetweenXml from(DateXml start, DateXml end) {
        BetweenXml betweenXml = new BetweenXml();
        betweenXml.start = start;
        betweenXml.end = end;
        return betweenXml;
    }

    public DateXml getEnd() {
        return end;
    }

    public void setEnd(DateXml end) {
        this.end = end;
    }

    public DateXml getStart() {
        return start;
    }

    public void setStart(DateXml start) {
        this.start = start;
    }
}
