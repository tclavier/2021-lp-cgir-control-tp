package fr.univlille.iut.cgir.api;

import redis.clients.jedis.Jedis;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/health/")
@Produces(MediaType.TEXT_PLAIN)
public class Health {
    @GET
    public String health() {
        Configuration configuration = Configuration.getInstance();
        String configStatus = configuration.useDefault() ? "default" : "system";
        String redisStatus = "";
        if (Configuration.NO_REDIS_SERVER.equals(configuration.getRedisHost())) {
            redisStatus = "Not present";
        } else {
            try {
                Jedis jedis = new Jedis(configuration.getRedisHost());
                jedis.set("foo", "bar");
                String value = jedis.get("foo");
                redisStatus = "Connected to " + configuration.getRedisHost();
            } catch (Exception e) {
                redisStatus = "Bad configuration";
                e.printStackTrace();
            }
        }
        return configuration.getServiceNane() + "\n\n" +
                "* Service : OK\n" +
                "* Properties : " + configStatus + "\n" +
                "* Redis : " + redisStatus + "\n";
    }
}
