FROM maven AS builder
WORKDIR /app
COPY pom.xml /app/
RUN mvn dependency:go-offline
COPY . /app/
RUN mvn package

FROM openjdk:11
WORKDIR /app
COPY --from=builder /app/target/ctp-1.0-jar-with-dependencies.jar /app/
EXPOSE 8080
CMD java -jar ctp-1.0-jar-with-dependencies.jar
