# Lancer le container

```shell
docker login registry.gitlab.com
docker run -p 8080:8080 -it registry.gitlab.com/tclavier/2021-lp-cgir-control-tp:latest

```

# Lancer en modifiant la conf

```shell
docker run -p 8080:8080 -it -v $(pwd)/config/:/etc/interval/ registry.gitlab.com/tclavier/2021-lp-cgir-control-tp:latest
```

# Lancer avec Redis

```shell
docker network create ctp
docker run -it --name redis --rm --network ctp redis
docker run -p 8080:8080 -it --network ctp -e REDIS_SERVER=redis registry.gitlab.com/tclavier/2021-lp-cgir-control-tp:latest
```


